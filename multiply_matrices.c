// Used for multiplying two matrices.


// matrix multiplication is done using ijk loop
void multiply_matrix_ijk(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	
	
	int i , j , k;
 	for(i=0; i < row1; i++) 
	{ 
		for(j=0; j < col2; j++) 
		{ 
	 		for(k=0; k<col1; k++)  
				matrix3[i][j] += matrix1[i][k] * matrix2[k][j]; 
		} 
	} 
	
}

// matrix multiplication is done using jik loop
void multiply_matrix_jik(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	
	
	int i , j , k;
	for(j=0; j < col2; j++) 
	{ 
		for(i=0; i < row1; i++) 
		{  
	 		for(k=0; k<col1; k++)  
				matrix3[i][j] += matrix1[i][k] * matrix2[k][j]; 			
		} 
	} 

}

// matrix multiplication is done using kij loop
void  multiply_matrix_kij(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	
	
	int i , j , k,intermidiate_values; 	
 	for(k=0; k < col1; k++) 
	{ 
		for(i=0; i < row1; i++) 
		{ 
			intermidiate_values=matrix1[i][k]; 							// intermidiate_values is created to increase the time efficiency
	 		for(j=0; j<col2; j++)  
				matrix3[i][j] += intermidiate_values * matrix2[k][j]; 
		} 
	} 
}

// matrix multiplication is done using ikj loop
void multiply_matrix_ikj(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	int i , j , k,intermidiate_values;	
 	for(i=0; i < row1; i++) 
	{ 
		for(k=0; k < col1; k++) 
		{ 
			intermidiate_values=matrix1[i][k]; 								// intermidiate_values is created to increase the time efficiency
	 		for(j=0; j<col2; j++)  
				matrix3[i][j] += intermidiate_values * matrix2[k][j]; 
		} 
	} 
}


// matrix multiplication is done using  jki loop
void  multiply_matrix_jki(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	
	
	int i , j , k,intermidiate_values;
 	
 	for(j=0; j < col2; j++) 
	{ 
		for(k=0; k< col1; k++) 
		{ 
			intermidiate_values=matrix2[k][j]; 							// intermidiate_values is created to increase the time efficiency
	 		for(i=0; i<row1; i++)  
				matrix3[i][j] += matrix1[i][k] * intermidiate_values; 
		} 
	} 
}

// matrix multiplication is done using kji loop
void multiply_matrix_kji(int row1 , int col1 , int row2 , int col2 , int** matrix1, int** matrix2,int** matrix3){
	
	
	
	int i , j , k,intermidiate_values;
 	for(k=0; k < col1; k++) 
	{ 
		for(j=0; j < col2; j++) 
		{ 
			intermidiate_values=matrix2[k][j]; 								// intermidiate_values is created to increase the time efficiency
	 		for(i=0; i<row1; i++)  
				matrix3[i][j] +=matrix1[i][k] * intermidiate_values; 
		} 
	} 
}




