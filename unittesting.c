#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "multiply_matrices.c"
int main(int argc, char  **argv)                   // Passing filename as arguments
{
	FILE *fptr,*fptr1;
	printf("%s",argv[1]);
    //char filename[100]="Test.txt";
    char filename[100];
    strcpy(filename,argv[1]);
    char ch;
    char c[10] , empty[1];
    fptr = fopen(filename, "r");
    int i,t=0,num=0;
    int arr[10];
    
	// Reading the dimenesion of the matrix form the file 
	for(t=0;t<4;)
	{
		ch=fgetc(fptr);
			if(isspace(ch))
			{
				arr[t]=num;
				num=0;
				t++;
				
			}
			else
			{
				if(ch == '-'){
					printf("Row or Col less than zero");
					exit(0);
				}
				else{
				num =(num*10) +(ch-'0');
				}
				
			}
	}
	
	int r1=arr[0],c1=arr[1],r2=arr[2],c2=arr[3];
	//printf("%d  %d  %d  %d",r1,c1,r2,c2);
	
	//	Verifying the dimmensions
	if(c1 != r2){
		printf("Row of second matrix not equal to Col of second matrix:");
		exit(0);
	}
	

 	//	Declaring  the matrices
 	int**  matrixA ;
	matrixA = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		matrixA[i] = (int *)malloc(sizeof(int) * c1);
 	
 	int**  matrixB ;
	matrixB = (int **) malloc(sizeof(int *) * r2);
	for(i=0; i<r2; i++)  
 		matrixB[i] = (int *)malloc(sizeof(int) * c2);
 		
 	int**  sam_result ;
	sam_result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		sam_result[i] = (int *)malloc(sizeof(int) * c2);


	//	Reading values from the file for matrixA
	num=0;
	ch=fgetc(fptr);
	int x , y , k;
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c1;)
		{
		
			ch=fgetc(fptr);
			if(isspace(ch))
			{
				num = atoi(c);
				
				matrixA[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
	}
	
	//printf("\n");


	//	Displaying values of matrixA

	//for(x=0;x<r1;x++)
	//{
	//	for (y=0;y<c1;y++)
	//	{
	//		printf("%d ",matrixA[x][y]);
	//	}
	//}

	//	Reading values from the file for matrixB

	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r2;x++)
	{
		for( y=0;y<c2;)
		{
			ch=fgetc(fptr);
			
			if(isspace(ch))
			{
				num = atoi(c);
				
				matrixB[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
		
	}
	//printf("\n");
	
	// Displaying values of matrixB
		
	//	for(x=0;x<r2;x++)
	//	{
	//		for(y=0;y<c2;y++)
	//		{
	//			printf("%d ",matrixB[x][y]);
	//		}
	//	}
	
	//	Reading values for resultant_matrix  from file 	
	num=0;
	ch=fgetc(fptr);
	for( x=0;x<r1;x++)
	{
		for( y=0;y<c2;)
		{
		ch=fgetc(fptr);
		if(isspace(ch))
			{
				num = atoi(c);
				
				sam_result[x][y]=num;
				strcpy(c , empty);
				num=0;
				y++;
			
			}
			else
			{
				strncat(c , &ch , 1);
			}
		}
	}
	//printf("\n");
	
	//	Displaying the values of sam_result
		
	//	for(x=0;x<r1;x++)
	//	{
	//		for(y=0;y<c2;y++)
	//		{
	//			printf("%d ",sam_result[x][y]);
	//		}
	//	}

	//	Initializing obtained_result matrix to store calculated value
	int** obtained_result;
	obtained_result = (int **) malloc(sizeof(int *) * r1);
	for(i=0; i<r1; i++)  
 		obtained_result[i] = (int *)malloc(sizeof(int) * c2); 
	
	for(x=0;x<r1;x++)
	{
		for(y=0;y<c2;y++)
		{
			obtained_result[x][y]=0;
		}
	}


	clock_t begin=clock();														//  to save the start_time 
	multiply_matrix_ikj(r1,c1,r2,c2,matrixA,matrixB,obtained_result);			//	matrixmultiplication function is called 
	clock_t end= clock();														//	end_time is saved												
	double time_spent =0.0;
	time_spent += (double)(end - begin) / CLOCKS_PER_SEC;						//	time_spent is calculated
	printf("\n");
	printf("%f\n", time_spent*1000);
	
	
	// Displaying the values of matrix3
	
//	for(x=0;x<r1;x++)
//	{
//		for(y=0;y<c2;y++)
//		{
//			printf("%d ",obtained_result[x][y]);
//		}
//	}
//	printf("\n");
	
	//	Validating sam0_result and obtained_result 
	int j;
	for( i=0;i<r1;i++)
	{
		for( j=0;j<c2;j++)
		{
			if(sam_result[i][j]!=obtained_result[i][j] )
			{
				printf("testcase failed\n");
				return 0; 
			}
		}
				
	}
	printf("testcase passed\n");
}