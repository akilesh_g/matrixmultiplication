import numpy as np
def multiply_matrices(matrix1 , matrix2):           

    result = np.dot(matrix1 , matrix2)          # multiplying two matrices
    return result                               # returning the result
   
    